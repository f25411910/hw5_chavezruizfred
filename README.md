# Práctica 5

**Universitario:** Fred Chavez Ruiz  
**Materia:** Diseño y Programación Gráfica

En la presente practica se subiran capturas de los primeros cinco niveles y los últimmos diez niveles del juego de CSS Dinner(Fluke Out), se puede acceder por el siguienete link(https://flukeout.github.io), las capturas son de los niveles: 1, 2, 3, 4, 5, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32. Adjunto una captura de todos niveles en general.

## Capturas 


### Level 1
![**Level 1**](/imagesFlukeOut/flukeOut_1.png)
### Level 2
![**Level 2**](/imagesFlukeOut/flukeOut_2.png)
### Level 3
![**Level 3**](/imagesFlukeOut/flukeOut_3.png)
### Level 4
![**Level 4**](/imagesFlukeOut/flukeOut_4.png)
### Level 5
![**Level 5**](/imagesFlukeOut/flukeOut_5.png)
### Level 22
![**Level 22**](/imagesFlukeOut/flukeOut_22.png)
### Level 23
![**Level 23**](/imagesFlukeOut/flukeOut_23.png)
### Level 24
![**Level 24**](/imagesFlukeOut/flukeOut_24.png)
### Level 25
![**Level 25**](/imagesFlukeOut/flukeOut_25.png)
### Level 26
![**Level 26**](/imagesFlukeOut/flukeOut_26.png)
### Level 27
![**Level 27**](/imagesFlukeOut/flukeOut_27.png)
### Level 28
![**Level 28**](/imagesFlukeOut/flukeOut_28.png)
### Level 29
![**Level 29**](/imagesFlukeOut/flukeOut_29.png)
### Level 30
![**Level 30**](/imagesFlukeOut/flukeOut_30.png)
### Level 31
![**Level 31**](/imagesFlukeOut/flukeOut_31.png)
### Level 32
![**Level 32**](/imagesFlukeOut/flukeOut_32.png)

### All levels
![**All levels**](/imagesFlukeOut/chooseLevel.png)
